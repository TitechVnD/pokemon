//
//  GameViewController.m
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/14/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import "GameViewController.h"
#import "PokemonHelpers.h"

@interface GameViewController ()
@property (weak, nonatomic) IBOutlet UIButton *startGameButton;

@end

@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    CGRect r = [[UIScreen mainScreen] bounds];
    CGRect rect = CGRectMake(0.0, r.size.height/2, r.size.width, self.startGameButton.frame.size.height/2);

    UIImageView *charmanderImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, rect.origin.y + rect.size.height, 60.0, 60.0)];
    UIImage *charmanderImage = [UIImage imageNamed:@"charmander.png"];
    [charmanderImageView setImage:charmanderImage];
    [self.view addSubview:charmanderImageView];
    CGRect charmanderImageRect = charmanderImageView.frame;
    [UIView animateWithDuration:3.0f
                          delay:0.0f
                        options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         [charmanderImageView setFrame:CGRectMake(self.view.frame.size.width - charmanderImageRect.size.width, charmanderImageRect.origin.y, charmanderImageRect.size.width, charmanderImageRect.size.height)];
                     }
                     completion:nil];

    CGRect charmanderRect = charmanderImageView.frame;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, charmanderRect.origin.y + charmanderRect.size.height - 15, 60.0, 60.0)];
    UIImage *image = [UIImage imageNamed:@"placeholder.png"];
    [imageView setImage:image];
    [self.view addSubview:imageView];
    [PokemonHelpers runSpinAnimationOnView:imageView duration:3.0f];
    CGRect imageRect = imageView.frame;
    [UIView animateWithDuration:3.0f
                          delay:0.0f
                        options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         [imageView setFrame:CGRectMake(self.view.frame.size.width - imageRect.size.width, imageRect.origin.y, imageRect.size.width, imageRect.size.height)];
                     }
                     completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    for (UIView *view in [self.view subviews]) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

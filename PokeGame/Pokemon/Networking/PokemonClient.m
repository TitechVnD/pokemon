//
//  PokemonClient.m
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/7/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import "PokemonClient.h"
#import <AFNetworking/AFNetworking.h>
#import "Pokemon.h"

NSString *const PokemonDataFetchSuccessNotification = @"PokemonDataFetchSuccessNotification";
NSString *const PokemonDataFetchFailureNotification = @"PokemonDataFetchFailureNotification";
NSString *const ChainDataFetchSuccessNotification = @"ChainDataFetchSuccessNotification";
NSString *const ChainDataFetchFailureNotification = @"ChainDataFetchFailureNotification";


@interface PokemonClient ()
{

}

@property (nonatomic) NSOperationQueue *operationQueue;

@end

@implementation PokemonClient

- (instancetype)init
{
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

- (instancetype)_init
{
    self = [super init];
    if (self)
    {
        self.operationQueue = [[NSOperationQueue alloc] init];
        self.chains = [NSMutableDictionary dictionary];
    }
    return self;
}

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static PokemonClient *instance;
    dispatch_once(&onceToken, ^{
        instance = [PokemonClient.alloc _init];
        atexit_b(^{
            [instance _cancelPendingOperations];
            instance = nil;
        });
    });
    return instance;
}

- (void)_cancelPendingOperations;
{
    [self.operationQueue cancelAllOperations];
}

- (void)fetchPokemons
{
    typeof(self) __weak weakSelf = self;
    NSURL *URL = [NSURL URLWithString:@"http://pokeapi.co/api/v2/pokedex/1/"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"JSON: %@", responseObject);
        typeof(weakSelf) __strong strongSelf = weakSelf;
        NSMutableArray *pokemons = [NSMutableArray array];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSArray *array = responseObject[@"pokemon_entries"];
            for (NSDictionary *dict in array) {
                NSUInteger pokemonId = [dict[@"entry_number"] unsignedIntegerValue];
                Pokemon *pokemon = [[Pokemon alloc] initWithId:pokemonId];
                NSDictionary *speciesDict = dict[@"pokemon_species"];
                pokemon.name = speciesDict[@"name"];
                NSString *spriteURL = [NSString stringWithFormat:@"http://pokeapi.co/media/sprites/pokemon/%lu.png",(unsigned long)pokemonId ];
                pokemon.spriteUrl = spriteURL;
                [pokemons addObject:pokemon];
            }
        }
        strongSelf.pokemons = [pokemons copy];
        [[NSNotificationCenter defaultCenter] postNotificationName:PokemonDataFetchSuccessNotification object:nil];

    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:PokemonDataFetchFailureNotification object:nil];
    }];
}

- (void)fetchChainWithChainId:(NSUInteger)chainId
{
    typeof(self) __weak weakSelf = self;
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://pokeapi.co/api/v2/evolution-chain/%lu/",(unsigned long)chainId]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        typeof(weakSelf) __strong strongSelf = weakSelf;
        if ([responseObject isKindOfClass:[NSDictionary class]])
        {
            if (responseObject[@"chain"]) {
                [strongSelf chainWithDictionary:responseObject[@"chain"] index:0 resultDictionary:[NSMutableDictionary dictionary] chainId:chainId];
            }
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:ChainDataFetchSuccessNotification object:nil];
        return;
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:ChainDataFetchFailureNotification object:nil];
        return;
    }];
}

- (void)chainWithDictionary:(NSDictionary *)dictionary index:(NSInteger)index resultDictionary:(NSMutableDictionary *)result chainId:(NSUInteger)chainId
{
    if (!dictionary && result) {
        [self.chains addEntriesFromDictionary:@{@(chainId):result}];
        return;
    }
    NSDictionary *species = dictionary[@"species"];
    NSString *name = species[@"name"];
    [result addEntriesFromDictionary:@{@(index):name}];
    NSArray *evolves = dictionary[@"evolves_to"];
    if (evolves.count == 0 && result) {
        [self.chains addEntriesFromDictionary:@{@(chainId):result}];
        return;
    }
    NSDictionary *nextDictionary = [evolves objectAtIndex:0];
    [self chainWithDictionary:nextDictionary index:index + 1 resultDictionary:result chainId:chainId];
}

@end

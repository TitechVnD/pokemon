//
//  PokemonClient.h
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/7/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import <Foundation/Foundation.h>

extern __attribute__((visibility ("default"))) NSString *const PokemonDataFetchSuccessNotification;
extern __attribute__((visibility ("default"))) NSString *const PokemonDataFetchFailureNotification;
extern __attribute__((visibility ("default"))) NSString *const ChainDataFetchSuccessNotification;
extern __attribute__((visibility ("default"))) NSString *const ChainDataFetchFailureNotification;

@interface PokemonClient : NSObject
@property (nonatomic) NSArray *pokemons;
@property (nonatomic) NSMutableDictionary *chains;

+ (instancetype)sharedInstance;

- (void)fetchPokemons;

- (void)fetchChainWithChainId:(NSUInteger)chainId;

@end

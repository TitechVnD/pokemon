//
//  PokemonCell.m
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/11/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import "PokemonCell.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <QuartzCore/QuartzCore.h>

@implementation PokemonCell

- (void)setPokemon:(Pokemon *)pokemon
{
    self.nameLabel.text = pokemon.name;
    self.idLabel.text = [NSString stringWithFormat:@"#%03lu",pokemon.pokemonId];

    self.spriteImage.layer.borderColor = [UIColor colorWithRed:0.45 green:0.36 blue:0.24 alpha:1.0].CGColor;
    self.spriteImage.layer.borderWidth = 1.0f;
    self.spriteImage.layer.cornerRadius = 5.0f;

    __weak UIImageView *weakImageView = self.spriteImage;
    [weakImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:pokemon.spriteUrl]]
                            placeholderImage:[UIImage imageNamed:@"placeholder"]
                                     success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
                                         UIImageView *strongImageView = weakImageView;
                                         if (!strongImageView) return;

                                         [UIView transitionWithView:strongImageView
                                                           duration:0.3
                                                            options:UIViewAnimationOptionTransitionCrossDissolve
                                                         animations:^{
                                                             strongImageView.image = image;
                                                         }
                                                         completion:nil];
                                     }
                                     failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                     }
     ];
}

@end

//
//  PokemonDetailCell.h
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/14/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Pokemon.h"

@interface PokemonDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *spriteImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *idLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentPositionLabel;

- (void)setPokemon:(Pokemon *)pokemon;

@end

//
//  PokemonDetailCell.m
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/14/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import "PokemonDetailCell.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <QuartzCore/QuartzCore.h>

@implementation PokemonDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setPokemon:(Pokemon *)pokemon
{
    self.nameLabel.text = pokemon.name;
    self.idLabel.text = [NSString stringWithFormat:@"#%03lu",pokemon.pokemonId];

    self.spriteImage.layer.borderColor = [UIColor blackColor].CGColor;
    self.spriteImage.layer.borderWidth = 1.0f;
    self.spriteImage.layer.cornerRadius = 5.0f;

    __weak UIImageView *weakImageView = self.spriteImage;
    [weakImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:pokemon.spriteUrl]]
                         placeholderImage:[UIImage imageNamed:@"placeholder"]
                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
                                      UIImageView *strongImageView = weakImageView;
                                      if (!strongImageView) return;

                                      [UIView transitionWithView:strongImageView
                                                        duration:0.3
                                                         options:UIViewAnimationOptionTransitionCrossDissolve
                                                      animations:^{
                                                          strongImageView.image = image;
                                                      }
                                                      completion:nil];
                                  }
                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                  }
     ];
}

@end

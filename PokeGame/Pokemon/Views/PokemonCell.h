//
//  PokemonCell.h
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/11/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Pokemon.h"

@interface PokemonCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *spriteImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *idLabel;

- (void)setPokemon:(Pokemon *)pokemon;
@end

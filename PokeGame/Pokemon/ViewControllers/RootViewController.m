//
//  RootViewController.m
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/9/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import "RootViewController.h"
#import "HomeViewController.h"
#import "CubeNavigationController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.menuPreferredStatusBarStyle = UIStatusBarStyleDefault;
    self.contentViewShadowColor = UIColor.blackColor;
    self.contentViewShadowOffset = CGSizeMake(0, 0);
    self.contentViewShadowOpacity = 0.6;
    self.contentViewShadowRadius = 12;
    self.contentViewShadowEnabled = YES;

    HomeViewController *homeVC= [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    self.contentViewController = [[CubeNavigationController alloc] initWithRootViewController:homeVC];

    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftMenuViewController"];
    self.delegate = self;
}

@end

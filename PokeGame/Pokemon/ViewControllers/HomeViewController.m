//
//  HomeViewController.m
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/12/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import "HomeViewController.h"
#import <FontAwesomeKit/FAKFontAwesome.h>
#import <QuartzCore/QuartzCore.h>
#import "PokemonHelpers.h"

@interface HomeViewController ()

@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    id barButtonItemAttributes = @{UITextAttributeFont: [FAKFontAwesome iconFontWithSize:UIFont.buttonFontSize]};
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:barButtonItemAttributes forState:UIControlStateNormal];

    /*
     * Make a swipe to the right open the menu.
     */
    UISwipeGestureRecognizer *leftMenuSwipe = [UISwipeGestureRecognizer.alloc initWithTarget:self.navigationItem.leftBarButtonItem.target
                                                                                      action:self.navigationItem.leftBarButtonItem.action];
    leftMenuSwipe.numberOfTouchesRequired = 1;
    leftMenuSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:leftMenuSwipe];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    CGRect rect = self.welcomeLabel.frame;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, rect.origin.y + rect.size.height, 60.0, 60.0)];
    UIImage *image = [UIImage imageNamed:@"placeholder.png"];
    [imageView setImage:image];
    [self.view addSubview:imageView];
    [PokemonHelpers runSpinAnimationOnView:imageView duration:3.0f];
    CGRect imageRect = imageView.frame;
    [UIView animateWithDuration:3.0f
                          delay:0.0f
                        options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         [imageView setFrame:CGRectMake(self.view.frame.size.width - imageRect.size.width, imageRect.origin.y, imageRect.size.width, imageRect.size.height)];
                     }
                     completion:nil];
}

@end

//
//  ImageToNameGameViewController.m
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/14/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import "ImageToNameGameViewController.h"
#import "PokemonClient.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "Pokemon.h"

@interface ImageToNameGameViewController ()
{
    NSInteger _count;
    NSArray *_optionIBArray;
    Pokemon *_chosenPokemon;
    NSInteger _score;
    NSInteger _highScore;
}


@property (weak, nonatomic) IBOutlet UIImageView *spriteImage;
@property (weak, nonatomic) IBOutlet UILabel *option1;
@property (weak, nonatomic) IBOutlet UILabel *option2;
@property (weak, nonatomic) IBOutlet UILabel *option3;
@property (weak, nonatomic) IBOutlet UILabel *option4;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (nonatomic) NSArray *baseArray;
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UIButton *button3;
@property (weak, nonatomic) IBOutlet UIButton *button4;
@property (weak, nonatomic) IBOutlet UILabel *highScoreLabel;
@property (weak, nonatomic) IBOutlet UIView *optionsBackground;

@end

@implementation ImageToNameGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _highScore = [[NSUserDefaults standardUserDefaults] integerForKey:@"HighScore"];
    _highScoreLabel.text = [NSString stringWithFormat:@"High Score : %ld",(long)_highScore];
    _score = 0;
    _scoreLabel.text = [NSString stringWithFormat:@"Score : %ld",(long)_score];
    _optionIBArray = [[NSArray alloc] initWithObjects:self.option1,self.option2,self.option3,self.option4,nil];


    self.optionsBackground.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.optionsBackground.layer.borderWidth = 2.0f;
    self.optionsBackground.layer.cornerRadius = 8.0f;

    _count = [PokemonClient sharedInstance].pokemons.count;
    if (_count > 0) {
        NSMutableArray *array = [NSMutableArray array];
        for (NSUInteger i = 0; i < _count; ++i)
            [array addObject:[NSNumber numberWithUnsignedInteger:i]];
        self.baseArray = [array copy];
        [self createQuiz];
    } else {
        [[PokemonClient sharedInstance] fetchPokemons];
        [NSNotificationCenter.defaultCenter addObserver:self
                                               selector:@selector(pokemonFetchSuccess:)
                                                   name:PokemonDataFetchSuccessNotification
                                                 object:nil];

        [NSNotificationCenter.defaultCenter addObserver:self
                                               selector:@selector(pokemonFetchFailure:)
                                                   name:PokemonDataFetchFailureNotification
                                                 object:nil];
    }

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (_score > _highScore) {
        [[NSUserDefaults standardUserDefaults] setInteger:_score forKey:@"HighScore"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)dealloc
{
    [NSNotificationCenter.defaultCenter removeObserver:self];
}

- (void)pokemonFetchSuccess:(NSNotification *)notification
{
    [SVProgressHUD showSuccessWithStatus:@"Fetched !"];
    _count = [PokemonClient sharedInstance].pokemons.count;
    if (_count > 0) {
        NSMutableArray *array = [NSMutableArray array];
        for (NSUInteger i = 0; i < _count; ++i)
            [array addObject:[NSNumber numberWithUnsignedInteger:i]];
        self.baseArray = [array copy];
        [self createQuiz];
    }
}

- (void)pokemonFetchFailure:(NSNotification *)notification
{
    [SVProgressHUD showErrorWithStatus:@"Failed to fetch pokemon data ! Try again, please !"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)createQuiz
{
    NSMutableArray *randSequence = [self.baseArray mutableCopy];
    NSMutableArray *array = [NSMutableArray array];
    for (int i = (int)_count-1; i > _count - 5; --i) {
        int r = arc4random_uniform(i + 1); // 0~i
        [array addObject:[randSequence objectAtIndex:r]];
        [randSequence exchangeObjectAtIndex:i withObjectAtIndex:r];
    }

    //random 0~3
    int random = arc4random_uniform(4);
    NSNumber *number = [array objectAtIndex:random];
    _chosenPokemon = [[PokemonClient sharedInstance].pokemons objectAtIndex:[number intValue]];
    __weak UIImageView *weakImageView = self.spriteImage;
    [weakImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_chosenPokemon.spriteUrl]]
                         placeholderImage:[UIImage imageNamed:@"placeholder"]
                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
                                      UIImageView *strongImageView = weakImageView;
                                      if (!strongImageView) return;

                                      [UIView transitionWithView:strongImageView
                                                        duration:0.3
                                                         options:UIViewAnimationOptionTransitionCrossDissolve
                                                      animations:^{
                                                          strongImageView.image = image;
                                                      }
                                                      completion:nil];
                                  }
                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                  }
     ];
    for (int i = 0; i < 4; i++) {
        UILabel *option = [_optionIBArray objectAtIndex:i];
        NSNumber *number = [array objectAtIndex:i];
        Pokemon *pokemon = [[PokemonClient sharedInstance].pokemons objectAtIndex:[number intValue]];
        option.text = pokemon.name;
    }
}

- (IBAction)buttonOnClick:(id)sender {
    NSString *str;
    UIButton *button = (UIButton *)sender;
    if (button == self.button1) {
        str = self.option1.text;
    } else if (button == self.button2) {
        str = self.option2.text;
    } else if (button == self.button3) {
        str = self.option3.text;
    } else {
        str = self.option4.text;
    }

    if ([str isEqualToString:_chosenPokemon.name]) {
        _score ++;
        _scoreLabel.text = [NSString stringWithFormat:@"Score : %ld",(long)_score];
    }
    [self createQuiz];
}

@end

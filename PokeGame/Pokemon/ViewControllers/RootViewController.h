//
//  RootViewController.h
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/9/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import <RESideMenu/RESideMenu.h>

@interface RootViewController : RESideMenu <RESideMenuDelegate>

@end

//
//  PokemonDetailViewController.m
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/13/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import "PokemonDetailViewController.h"
#import <FontAwesomeKit/FAKFontAwesome.h>
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "PokemonHelpers.h"
#import "PokemonClient.h"
#import "PokemonDetailCell.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface PokemonDetailViewController ()

@property (nonatomic) NSMutableArray *evols;

@end

@implementation PokemonDetailViewController

- (instancetype)initWithPokemon:(Pokemon *)pokemon
{
    UIStoryboard* sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    PokemonDetailViewController* vc = [sb instantiateViewControllerWithIdentifier:@"PokemonDetailViewController"];
    vc.pokemon = pokemon;
    self.evols = [NSMutableArray array];
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [SVProgressHUD showWithStatus:@"Loading..."];

    self.evolutionTableView.rowHeight = UITableViewAutomaticDimension;
    self.evolutionTableView.estimatedRowHeight = 44;
    //[self.evolutionTableView registerClass:[PokemonDetailCell class] forCellReuseIdentifier:@"PokemonCell"];
    [self.evolutionTableView registerNib:[UINib nibWithNibName:@"PokemonDetailCell" bundle:nil] forCellReuseIdentifier:@"PokemonDetailCell"];

    /*
     * Make a swipe to the right open the menu.
     */
    UISwipeGestureRecognizer *leftMenuSwipe = [UISwipeGestureRecognizer.alloc initWithTarget:self.navigationItem.leftBarButtonItem.target
                                                                                      action:self.navigationItem.leftBarButtonItem.action];
    leftMenuSwipe.numberOfTouchesRequired = 1;
    leftMenuSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:leftMenuSwipe];

    [NSNotificationCenter.defaultCenter addObserver:self
                                           selector:@selector(chainFetchSuccess:)
                                               name:ChainDataFetchSuccessNotification
                                             object:nil];

    [NSNotificationCenter.defaultCenter addObserver:self
                                           selector:@selector(chainFetchFailure:)
                                               name:ChainDataFetchFailureNotification
                                             object:nil];

    self.spriteImageView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.spriteImageView.layer.borderWidth = 2.0f;
    self.spriteImageView.layer.cornerRadius = 8.0f;

    __weak UIImageView *weakImageView = self.spriteImageView;
    [weakImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.pokemon.spriteUrl]]
                         placeholderImage:[UIImage imageNamed:@"placeholder"]
                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
                                      UIImageView *strongImageView = weakImageView;
                                      if (!strongImageView) return;

                                      [UIView transitionWithView:strongImageView
                                                        duration:0.3
                                                         options:UIViewAnimationOptionTransitionCrossDissolve
                                                      animations:^{
                                                          [SVProgressHUD dismiss];
                                                          strongImageView.image = image;
                                                      }
                                                      completion:nil];
                                  }
                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                      [SVProgressHUD dismiss];
                                  }
     ];

    self.nameLabel.text = self.pokemon.name;
    self.idLabel.text = [NSString stringWithFormat:@"#%03lu",self.pokemon.pokemonId];

    self.firstTypeLabel.hidden = YES;
    self.secondTypeLabel.hidden = YES;
    self.firstTypeLabel.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.firstTypeLabel.layer.borderWidth = 1.0f;
    self.firstTypeLabel.layer.cornerRadius = 5.0f;
    self.secondTypeLabel.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.secondTypeLabel.layer.borderWidth = 1.0f;
    self.secondTypeLabel.layer.cornerRadius = 5.0f;

    typeof(self) __weak weakSelf = self;
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSURL *pokemonURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://pokeapi.co/api/v2/pokemon/%lu/",(unsigned long)self.pokemon.pokemonId]];
    [manager GET:pokemonURL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        if ([responseObject[@"types"] isKindOfClass:[NSArray class]]) {
            NSMutableArray *types = [NSMutableArray array];
            for (NSDictionary *dict in responseObject[@"types"]) {
                NSDictionary *typeDict = dict[@"type"];
                [types addObject:typeDict[@"name"]];
            }
            typeof(weakSelf) __strong strongSelf = weakSelf;
            dispatch_async(dispatch_get_main_queue(), ^{
                if (types.count == 0) {
                    strongSelf.firstTypeLabel.hidden = YES;
                    strongSelf.secondTypeLabel.hidden = YES;
                } else if (types.count == 1) {
                    strongSelf.firstTypeLabel.hidden = NO;
                    strongSelf.firstTypeLabel.text = [types objectAtIndex:0];
                    strongSelf.firstTypeLabel.backgroundColor = [PokemonHelpers colorWithString:[types objectAtIndex:0]];
                    strongSelf.secondTypeLabel.hidden = YES;
                } else  {
                    strongSelf.firstTypeLabel.hidden = NO;
                    strongSelf.firstTypeLabel.text = [types objectAtIndex:0];
                    strongSelf.secondTypeLabel.hidden = NO;
                    strongSelf.secondTypeLabel.text = [types objectAtIndex:1];
                    strongSelf.firstTypeLabel.backgroundColor = [PokemonHelpers colorWithString:[types objectAtIndex:0]];
                    strongSelf.secondTypeLabel.backgroundColor = [PokemonHelpers colorWithString:[types objectAtIndex:1]];
                }
            });
        }

    } failure:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.evolutionTableView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.evolutionTableView.layer.borderWidth = 1.0f;
    self.evolutionTableView.layer.cornerRadius = 5.0f;
    NSMutableArray *evols = [NSMutableArray array];
    NSDictionary * chain = [[PokemonClient sharedInstance].chains objectForKey:@(self.pokemon.evolutionChainId)];
    if (!chain) {
        [[PokemonClient sharedInstance] fetchChainWithChainId:self.pokemon.evolutionChainId];
    } else {
        for (NSNumber *key in chain) {
            [evols addObject:[chain objectForKey:key]];
        }
        _evols = [evols mutableCopy];
        [self.evolutionTableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [NSNotificationCenter.defaultCenter removeObserver:self];
}

- (void)chainFetchSuccess:(NSNotification *)notification
{
    //[SVProgressHUD showSuccessWithStatus:@"Fetched !"];
    NSMutableArray *evols = [NSMutableArray array];
    NSDictionary * chain = [[PokemonClient sharedInstance].chains objectForKey:@(self.pokemon.evolutionChainId)];
    for (NSNumber *key in chain) {
        NSString *name = [chain objectForKey:key];
        [evols addObject:name];
    }
    _evols = [evols mutableCopy];
    [self.evolutionTableView reloadData];
}

- (void)chainFetchFailure:(NSNotification *)notification
{
    [SVProgressHUD showErrorWithStatus:@"Failed to fetch pokemon data ! Try again, please !"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.evols.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_evols.count == 0) {
        return nil;
    }
    NSString *name = [_evols objectAtIndex:indexPath.section];
    for (Pokemon *pokemon in [PokemonClient sharedInstance].pokemons) {
        if ([pokemon.name isEqualToString:name]) {
            PokemonDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PokemonDetailCell" forIndexPath:indexPath];
            [cell setPokemon:pokemon];
            if (pokemon.pokemonId == self.pokemon.pokemonId) {
                cell.currentPositionLabel.hidden = NO;
            }
            return cell;
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 21;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect rect = self.evolutionTableView.frame;
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, rect.size.width, 21.0)];
    UILabel *headerTitle = [[UILabel alloc] initWithFrame:headerView.frame];
    headerTitle.textColor = [UIColor colorWithRed:0.51 green:0.46 blue:0.35 alpha:1.0];
    headerTitle.backgroundColor = [UIColor lightGrayColor];
    headerTitle.textAlignment = NSTextAlignmentCenter;
    if (section == 0) {
        headerTitle.text = @"Base Pokemon";
    } else {
        headerTitle.text = @"Evols to";
    }
    [headerView addSubview:headerTitle];
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *name = [_evols objectAtIndex:indexPath.section];
    if ([name isEqualToString:self.pokemon.name]) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    for (Pokemon *pokemon in [PokemonClient sharedInstance].pokemons) {
        if ([pokemon.name isEqualToString:name]) {
            [SVProgressHUD showWithStatus:@"Loading..."];
            tableView.userInteractionEnabled = NO;
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://pokeapi.co/api/v2/pokemon-species/%lu/",(unsigned long)pokemon.pokemonId]];
            [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                if ([responseObject isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *evolutionChain = responseObject[@"evolution_chain"];
                    if (evolutionChain[@"url"]) {
                        [SVProgressHUD dismiss];
                        tableView.userInteractionEnabled = YES;
                        NSString *urlString = evolutionChain[@"url"];
                        NSURL *path = [NSURL URLWithString:urlString];
                        pokemon.evolutionChainId = [PokemonHelpers unsignedIntegerWithObject:path.lastPathComponent];
                        PokemonDetailViewController *vc = [[PokemonDetailViewController alloc] initWithPokemon:pokemon];
                        [self.navigationController pushViewController:vc animated:YES];
                    }
                }

            } failure:^(NSURLSessionTask *operation, NSError *error) {
                PokemonDetailViewController *vc = [[PokemonDetailViewController alloc] initWithPokemon:pokemon];
                [self.navigationController pushViewController:vc animated:YES];
            }];
        }
    }
}


@end

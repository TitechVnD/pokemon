//
//  PokemonDetailViewController.h
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/13/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Pokemon.h"

@interface PokemonDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *spriteImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *idLabel;
@property (weak, nonatomic) IBOutlet UITableView *evolutionTableView;
@property (nonatomic) Pokemon* pokemon;

- (instancetype)initWithPokemon:(Pokemon *)pokemon;

@end

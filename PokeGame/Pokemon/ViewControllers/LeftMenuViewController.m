//
//  LeftMenuViewController.m
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/9/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import "LeftMenuViewController.h"
#import <FontAwesomeKit/FAKFontAwesome.h>
#import "HomeViewController.h"
#import "PokemonTableViewController.h"
#import "GameViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "CubeNavigationController.h"


@interface LeftMenuViewController ()

@end

static const CGFloat LeftMenuViewControllerFontSize  = 24;
static const CGFloat LeftMenuViewControllerIconRelativeSize  = 1.1;
static const CGFloat LeftMenuViewControllerRowHeight = LeftMenuViewControllerFontSize * 2;

@implementation LeftMenuViewController
- (void)viewDidLoad
{
    [super viewDidLoad];

    CGFloat tableHeight = self.menuLabels_.count * LeftMenuViewControllerRowHeight;
    UITableView *tableView = [UITableView.alloc initWithFrame:CGRectMake(16,
                                                                         (self.view.frame.size.height - tableHeight) * 0.5,
                                                                         self.view.frame.size.width - 64,
                                                                         tableHeight)
                                                        style:UITableViewStylePlain];
    tableView.delegate   = self;
    tableView.dataSource = self;

    tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    tableView.opaque           = NO;
    tableView.backgroundColor  = UIColor.clearColor;
    tableView.backgroundView   = nil;
    tableView.separatorStyle   = UITableViewCellSeparatorStyleNone;
    tableView.bounces          = NO;
    tableView.scrollsToTop     = NO;
    tableView.rowHeight        = LeftMenuViewControllerRowHeight;

    [self.view addSubview:tableView];
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0: // Show Home View
        {
            HomeViewController *homeVC= [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            CubeNavigationController *navigationController = (CubeNavigationController *) self.sideMenuViewController.contentViewController;
            [navigationController pushViewController:homeVC animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
            break;

        case 1: // Show Pokemon Table View
        {
            PokemonTableViewController *pokemonTableVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PokemonTableViewController"];
            CubeNavigationController *navigationController = (CubeNavigationController *) self.sideMenuViewController.contentViewController;
            [navigationController pushViewController:pokemonTableVC animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
            break;

        case 2: // Show Game View
        {
            GameViewController *gameVC = [self.storyboard instantiateViewControllerWithIdentifier:@"GameViewController"];
            CubeNavigationController *navigationController = (CubeNavigationController *) self.sideMenuViewController.contentViewController;
            [navigationController pushViewController:gameVC animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
            break;

        case 3: // Show About
        {
            static UIImage *infoImage;
            static dispatch_once_t once;
            dispatch_once(&once, ^
                          {
                              FAKFontAwesome *infoIcon = [FAKFontAwesome infoCircleIconWithSize:62];
                              [infoIcon setAttributes:@{NSForegroundColorAttributeName: UIColor.whiteColor}];
                              infoImage = [infoIcon imageWithSize:CGSizeMake(56, 56)];
                          });
            [SVProgressHUD showImage:infoImage status:@"PokemonApp v1.0.0"];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark UITableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return self.menuLabels_.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"menuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell)
    {
        cell = [UITableViewCell.alloc initWithStyle:UITableViewCellStyleDefault
                                    reuseIdentifier:cellIdentifier];

        cell.selectedBackgroundView         = UIView.new;
        cell.backgroundColor                = UIColor.clearColor;
        cell.textLabel.textColor            = UIColor.darkGrayColor;
        cell.textLabel.highlightedTextColor = UIColor.lightGrayColor;
    }

    cell.textLabel.attributedText = self.menuLabels_[indexPath.row];

    return cell;
}

#pragma mark - Private methods

- (NSArray *)menuLabels_
{
    static NSArray *labels;
    static dispatch_once_t once;
    dispatch_once(&once, ^
                  {
                      const CGFloat textFontSize = LeftMenuViewControllerFontSize;
                      UIFont *textFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:textFontSize];

                      const CGFloat iconFontSize = textFont.lineHeight * LeftMenuViewControllerIconRelativeSize;
                      const CGFloat baselineOffset = .5 * (textFontSize - iconFontSize);
                      UIFont *iconFont = [FAKFontAwesome iconFontWithSize:iconFontSize];


                      NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
                      paragraphStyle.minimumLineHeight = iconFont.lineHeight - baselineOffset;

                      NSDictionary *textAttributes = @{NSFontAttributeName: textFont,
                                                       NSParagraphStyleAttributeName: paragraphStyle};

                      NSDictionary *iconAttributes = @{NSFontAttributeName: iconFont,
                                                       NSParagraphStyleAttributeName: paragraphStyle,
                                                       NSBaselineOffsetAttributeName: @(baselineOffset)};

                      labels = ({
                          NSMutableArray *builder = NSMutableArray.new;
                          for (NSString *labelString in @[@"\uf015 Home",
                                                          @"\uf03a List",
                                                          @"\uf11b Game",
                                                          @"\uf129 About"
                                                          ])
                          {
                              NSMutableAttributedString *attributed = [NSMutableAttributedString.alloc initWithString:labelString];
                              [attributed setAttributes:iconAttributes range:NSMakeRange(0, 1)];
                              [attributed setAttributes:textAttributes range:NSMakeRange(1, labelString.length - 1)];
                              [builder addObject:attributed];
                          }
                          builder.copy;
                      });
                  });
    
    return labels;
}

@end


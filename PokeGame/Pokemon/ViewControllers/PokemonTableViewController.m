//
//  PokemonTableViewController.m
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/9/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import "PokemonTableViewController.h"
#import <FontAwesomeKit/FAKFontAwesome.h>
#import <AFNetworking/AFNetworking.h>
#import "Pokemon.h"
#import "PokemonCell.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "PokemonClient.h"
#import "PokemonDetailViewController.h"
#import "PokemonHelpers.h"
//#import "CubeNavigationController.h"

@interface PokemonTableViewController ()

@property (nonatomic) NSArray *pokemons;

@end

@implementation PokemonTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([PokemonClient sharedInstance].pokemons.count != 0) {
        _pokemons = [[PokemonClient sharedInstance].pokemons copy];
    } else {
        [SVProgressHUD showWithStatus:@"Fetching..."];
        [[PokemonClient sharedInstance] fetchPokemons];
    }

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;

    /*
     * Navigation items use an icon font.
     */
    id barButtonItemAttributes = @{UITextAttributeFont: [FAKFontAwesome iconFontWithSize:UIFont.buttonFontSize]};
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:barButtonItemAttributes forState:UIControlStateNormal];

    /*
     * Make a swipe to the right open the menu.
     */
    UISwipeGestureRecognizer *leftMenuSwipe = [UISwipeGestureRecognizer.alloc initWithTarget:self.navigationItem.leftBarButtonItem.target
                                                                                      action:self.navigationItem.leftBarButtonItem.action];
    leftMenuSwipe.numberOfTouchesRequired = 1;
    leftMenuSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:leftMenuSwipe];

    [NSNotificationCenter.defaultCenter addObserver:self
                                           selector:@selector(pokemonFetchSuccess:)
                                               name:PokemonDataFetchSuccessNotification
                                             object:nil];

    [NSNotificationCenter.defaultCenter addObserver:self
                                           selector:@selector(pokemonFetchFailure:)
                                               name:PokemonDataFetchFailureNotification
                                             object:nil];
}

- (void)pokemonFetchSuccess:(NSNotification *)notification
{
    [SVProgressHUD showSuccessWithStatus:@"Fetched !"];
    _pokemons = [[PokemonClient sharedInstance].pokemons copy];
    [self.tableView reloadData];
}

- (void)pokemonFetchFailure:(NSNotification *)notification
{
    [SVProgressHUD showErrorWithStatus:@"Failed to fetch pokemon data ! Try again, please !"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)dealloc
{
    [NSNotificationCenter.defaultCenter removeObserver:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_pokemons count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_pokemons.count == 0) {
        return nil;
    }
    PokemonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PokemonCell" forIndexPath:indexPath];
    Pokemon *pokemon = [_pokemons objectAtIndex:indexPath.row];
    [cell setPokemon:pokemon];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 62;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Pokemon *pokemon = [_pokemons objectAtIndex:indexPath.row];
    [SVProgressHUD showWithStatus:@"Loading..."];
    tableView.userInteractionEnabled = NO;
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://pokeapi.co/api/v2/pokemon-species/%lu/",(unsigned long)pokemon.pokemonId]];
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *evolutionChain = responseObject[@"evolution_chain"];
            if (evolutionChain[@"url"]) {
                [SVProgressHUD dismiss];
                tableView.userInteractionEnabled = YES;
                NSString *urlString = evolutionChain[@"url"];
                NSURL *path = [NSURL URLWithString:urlString];
                pokemon.evolutionChainId = [PokemonHelpers unsignedIntegerWithObject:path.lastPathComponent];
                PokemonDetailViewController *vc = [[PokemonDetailViewController alloc] initWithPokemon:pokemon];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }

    } failure:^(NSURLSessionTask *operation, NSError *error) {
        PokemonDetailViewController *vc = [[PokemonDetailViewController alloc] initWithPokemon:pokemon];
        [self.navigationController pushViewController:vc animated:YES];
    }];

}

@end

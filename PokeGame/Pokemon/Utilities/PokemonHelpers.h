//
//  PokemonHelpers.h
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/7/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PokemonHelpers : NSObject

+ (int64_t)integerWithObject:(nullable id)object;

+ (uint64_t)unsignedIntegerWithObject:(nullable id)object;

+ (nullable NSString *)stringWithObject:(nullable id)object;

+ (double)doubleWithObject:(nullable id)object;

+ (UIColor * _Nonnull)colorWithString:(nonnull NSString *)string;

+ (void)runSpinAnimationOnView:(nonnull UIView *)view duration:(CGFloat)duration;

@end

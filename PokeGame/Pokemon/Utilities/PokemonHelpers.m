//
//  PokemonHelpers.m
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/7/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import "PokemonHelpers.h"

@implementation PokemonHelpers

+ (int64_t)integerWithObject:(id)object
{
    if ([object isKindOfClass:NSNumber.class])
    {
        NSNumber *numberValue = object;
        return numberValue.longLongValue;
    }
    else if ([object isKindOfClass:NSNull.class])
    {
        object = nil;
    }
    else if ([object isKindOfClass:NSString.class])
    {
        NSString *stringValue = object;
        if (stringValue.length)
        {
            char *invalid;
            int64_t value = strtoll(stringValue.UTF8String, &invalid, 10);
            if (!*invalid)
            {
                return value;
            }
            if ([stringValue isEqualToString:@"true"])  { return 1ll; }
            if ([stringValue isEqualToString:@"false"]) { return 0ll; }
        }
        else
        {
            // Empty string is treated as missing value
            object = nil;
        }
    }

    if (object && ![object isKindOfClass:NSNull.class])
    {
        [NSException raise:NSInvalidArgumentException format:@"Doesn't represent an integer: %@", object];
    }

    return INT64_MAX;
}

+ (uint64_t)unsignedIntegerWithObject:(id)object
{
    if ([object isKindOfClass:NSNumber.class])
    {
        NSNumber *numberValue = object;
        return numberValue.unsignedLongLongValue;
    }
    else if ([object isKindOfClass:NSNull.class])
    {
        object = nil;
    }
    else if ([object isKindOfClass:NSString.class])
    {
        NSString *stringValue = object;
        if (stringValue.length)
        {
            char *invalid;
            uint64_t value = strtoull(stringValue.UTF8String, &invalid, 10);
            if (!*invalid)
            {
                return value;
            }
            if ([stringValue isEqualToString:@"true"])  { return 1ull; }
            if ([stringValue isEqualToString:@"false"]) { return 0ull; }
        }
        else
        {
            // Empty string is treated as missing value
            object = nil;
        }
    }

    if (object && ![object isKindOfClass:NSNull.class])
    {
        [NSException raise:NSInvalidArgumentException format:@"Doesn't represent an integer: %@", object];
    }

    return UINT64_MAX;
}

+ (NSString *)stringWithObject:(id)object
{
    if (![object isKindOfClass:NSString.class])
    {
        if ([object isKindOfClass:NSNull.class])
        {
            return nil;
        }
        else if ([object respondsToSelector:@selector(stringValue)])
        {
            object = [object stringValue];
        }
        else if (object)
        {
            [NSException raise:NSInvalidArgumentException format:@"Cannot be coerced to a NSString: %@", object];
            object = nil;
        }
    }

    return [object length] ? object : nil;
}

+ (double)doubleWithObject:(id)object
{
    if ([object isKindOfClass:NSNumber.class])
    {
        NSNumber *numberValue = object;
        return numberValue.doubleValue;
    }
    else if ([object isKindOfClass:NSNull.class])
    {
        object = nil;
    }
    else if ([object isKindOfClass:NSString.class])
    {
        NSString *stringValue = object;
        if (stringValue.length)
        {
            char *invalid;
            double value = strtod(stringValue.UTF8String, &invalid);
            if (!*invalid)
            {
                return value;
            }
        }
        else
        {
            object = nil;
        }
    }
    if (object && ![object isKindOfClass:NSNull.class])
    {
        [NSException raise:NSInvalidArgumentException format:@"Doesn't represent a double: %@", object];
    }
    return NAN;
}

+ (UIColor *)colorWithString:(NSString *)string
{
    if ([string isEqualToString:@"normal"]) {
        return [UIColor colorWithRed:0.73 green:0.68 blue:0.43 alpha:1.0];
    } else if ([string isEqualToString:@"fighting"]) {
        return [UIColor colorWithRed:0.83 green:0.18 blue:0.04 alpha:1.0];
    } else if ([string isEqualToString:@"flying"]) {
        return [UIColor colorWithRed:0.79 green:0.67 blue:0.89 alpha:1.0];
    } else if ([string isEqualToString:@"poison"]) {
        return [UIColor colorWithRed:0.64 green:0.02 blue:0.79 alpha:1.0];
    } else if ([string isEqualToString:@"ground"]) {
        return [UIColor colorWithRed:0.76 green:0.72 blue:0.34 alpha:1.0];
    } else if ([string isEqualToString:@"rock"]) {
        return [UIColor colorWithRed:0.52 green:0.49 blue:0.05 alpha:1.0];
    } else if ([string isEqualToString:@"bug"]) {
        return [UIColor colorWithRed:0.40 green:0.52 blue:0.04 alpha:1.0];
    } else if ([string isEqualToString:@"steel"]) {
        return [UIColor colorWithRed:0.52 green:0.54 blue:0.51 alpha:1.0];
    } else if ([string isEqualToString:@"fire"]) {
        return [UIColor colorWithRed:0.92 green:0.50 blue:0.27 alpha:1.0];
    } else if ([string isEqualToString:@"grass"]) {
        return [UIColor colorWithRed:0.12 green:0.77 blue:0.39 alpha:1.0];
    } else if ([string isEqualToString:@"electric"]) {
        return [UIColor colorWithRed:0.97 green:0.89 blue:0.33 alpha:1.0];
    } else if ([string isEqualToString:@"ice"]) {
        return [UIColor colorWithRed:0.47 green:0.89 blue:0.85 alpha:1.0];
    } else if ([string isEqualToString:@"dark"]) {
        return [UIColor colorWithRed:0.34 green:0.24 blue:0.24 alpha:1.0];
    } else if ([string isEqualToString:@"fairy"]) {
        return [UIColor colorWithRed:0.89 green:0.55 blue:0.76 alpha:1.0];
    } else if ([string isEqualToString:@"water"]) {
        return [UIColor colorWithRed:0.14 green:0.48 blue:0.81 alpha:1.0];
    } else if ([string isEqualToString:@"shadow"]) {
        return [UIColor colorWithRed:0.66 green:0.47 blue:0.47 alpha:1.0];
    } else if ([string isEqualToString:@"psychic"]) {
        return [UIColor colorWithRed:0.81 green:0.14 blue:0.55 alpha:1.0];
    } else if ([string isEqualToString:@"ghost"]) {
        return [UIColor colorWithRed:0.27 green:0.14 blue:0.51 alpha:1.0];
    } else if ([string isEqualToString:@"dragon"]) {
        return [UIColor colorWithRed:0.44 green:0.01 blue:0.80 alpha:1.0];
    } else {
        // unknown
        return [UIColor colorWithRed:0.94 green:0.89 blue:0.89 alpha:1.0];
    }
}

+ (void)runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = @(M_PI * 4.0);
    rotationAnimation.duration = duration;
    rotationAnimation.repeatCount = HUGE_VAL;
    rotationAnimation.autoreverses = YES;
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

@end

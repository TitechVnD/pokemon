//
//  Pokemon.h
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/6/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pokemon : NSObject <NSCopying>

@property (nonatomic) NSUInteger pokemonId;
@property (nonatomic) NSUInteger evolutionChainId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *spriteUrl;
@property (nonatomic, copy) NSArray *types;

- (instancetype)initWithId:(NSUInteger)pokemonId;

@end

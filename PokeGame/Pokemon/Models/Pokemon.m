//
//  Pokemon.m
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/6/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import "Pokemon.h"

static BOOL objects_equal(id objA, id objB)
{
    return (!objA && !objB) || (objA && objB && [objA isEqual:objB]);
}

@implementation Pokemon

- (instancetype)initWithId:(NSUInteger)pokemonId
{
    if (self == [super init])
    {
        _pokemonId = pokemonId;
    }
    return self;
}


- (NSUInteger)hash
{
    return self.pokemonId ^ self.evolutionChainId ^ self.name.hash ^ self.spriteUrl.hash ^ self.types.hash;
}

- (BOOL)isEqual:(id)object
{
    if (self == object)
    {
        return YES;
    }
    else if (![object isKindOfClass:[self class]] || (self.hash != [object hash]))
    {
        return NO;
    }
    else
    {
        Pokemon *other = object;
        return objects_equal(self.name, other.name) && objects_equal(self.spriteUrl, other.spriteUrl) && objects_equal(self.types, other.types) && (self.pokemonId == other.pokemonId) && (self.evolutionChainId == other.evolutionChainId);
    }
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone
{
    Pokemon *copy = [[[self class] allocWithZone:zone] initWithId:self.pokemonId];
    copy.name = self.name;
    copy.evolutionChainId = self.evolutionChainId;
    copy.spriteUrl = self.spriteUrl;
    copy.types = [self.types copy];
    return copy;
}

@end

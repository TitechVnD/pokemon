//
//  AppDelegate.h
//  Pokemon
//
//  Created by Ta, Viet | Vito | GHRD on 6/6/16.
//  Copyright © 2016 Ta, Viet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

